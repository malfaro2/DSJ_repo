# DSJ Repo

Repository for code and data necessary to reproduce and replicate the paper: "A School and a Network: CODATA-RDA Data Science Summer Schools Alumni Survey".

# Introduction

This is the code for the paper :

Bezuidenhout, L, DrummondCurtis, S, Walker, B, Shanahan, H and Alfaro-Córdoba, M. 2021. A School and a Network: CODATA-RDA Data Science Summer Schools Alumni Survey. Data Science Journal, 19: XX, pp. 1–12. DOI: https://doi.org/10.5334/dsj-2021-007

# Contents:

- Data
- Code: R code to replicate the plots included in the article.
- Plots: as they appear in the article.
- Session Info: R packages and versions needed to run the code.

# Cite:

If you find this code useful in your research, please, consider citing our paper.
